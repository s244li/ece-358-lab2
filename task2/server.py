import socket

# setup memory with correct ip adresses
def ip_data (web):
    if(web=="google.com"):
        IP()
        IP.TTL = "01 04 "
        # requested_IP = 192.165.1.1, 192.165.1.10
        IP.hex_IP = "c0 a5 01 01 "
        IP.hex_IP2 = "c0 a5 01 0a "
        IP.RDLENGTH = "04 "
        return IP
    elif(web=="youtube.com"):
        IP()
        IP.TTL = "00 a0 "
        IP.hex_IP = "c0 a5 01 02 "
        IP.hex_IP2 = "0"
        IP.RDLENGTH = "04 "
        return IP      
    elif(web=="uwaterloo.ca"):
        IP()
        IP.TTL = "00 a0 "
        IP.hex_IP = "c0 a5 01 03 "
        IP.hex_IP2 = "0"
        IP.RDLENGTH = "04 "
        return IP
    elif(web=="wikipedia.org"):
        IP()
        IP.TTL = "00 a0 "
        IP.hex_IP = "c0 a5 01 04 "
        IP.hex_IP2 = "0"
        IP.RDLENGTH = "04 "
        return IP
    elif(web=="amazon.ca"):
        IP()
        IP.TTL = "00 a0 "
        IP.hex_IP = "c0 a5 01 05 "
        IP.hex_IP2 = "0"
        IP.RDLENGTH = "04 "
        return IP
    else:
        IP()
        IP.hex_IP = "0"
        return IP



# open UDP socket (as server)
my_IP = "127.0.0.1"
my_port = 12000
buffer_size = 2048
ServerSocket = socket.socket(
    family=socket.AF_INET, type=socket.SOCK_DGRAM)  # create UDP socket
ServerSocket.bind((my_IP, my_port))  # bind socket to port and IP
print("UDP server ready")




while (True):
    # ---------------------listen for request----------------------------------
    input_from_client = ServerSocket.recvfrom(buffer_size)
    msg_c2s = input_from_client[0]
    client_IP = input_from_client[1]

    request = msg_c2s.decode().upper()  # "Message from Client:{}".format(msg_c2s)
    print("request:")
    print(request)

    # ---------------------generate response to request-------------------------
    # generate header
    ID = request[0:5] + " "
    FLAGS = "84 00 "  # QR = 0,OPCODE = 0,AA = 1,TC=0,RD=0,RA=0, Z=000, RCODE=0 == ob0000010000000000 = ox 0400
    QDCOUNT = "00 01 "
    ANCOUNT = "00 02 "
    NSCOUNT = "00 00 "
    ARCOUNT = "00 00 "
    HEADER = ID + FLAGS + QDCOUNT + ANCOUNT + NSCOUNT + ARCOUNT

    # generate answer 
    NAME = "c0 0c "
    TYPE = "00 01 "
    CLASS = "00 01 "
    websitename = ""
    letter = ""
    class IP:
        def __init__(self):
            self.TTL = ""
            self.RDLENGTH = ""
            self.RDATA = ""
            self.RDATA2 = ""
            self.hex_IP = ""
            self.hex_IP2 = ""


    # parse information
    requested_IP = request[36:]
    first_lable_length = int(requested_IP[:2], 16)
    i=0
    while (i < first_lable_length):
        i += 1
        letter = requested_IP[i*3:i*3+2]
        letter_byte = bytes.fromhex(letter)
        letter_char = letter_byte.decode()
        websitename = websitename + letter_char
    websitename = websitename + "."
    second_lable_length = int(requested_IP[(first_lable_length+1)*3:(first_lable_length+1)*3+2], 16)    
    i=0
    while (i < second_lable_length):
        i += 1
        letter = requested_IP[((i+first_lable_length+1)*3):((i+first_lable_length+1)*3+2)]
        letter_byte = bytes.fromhex(letter)
        letter_char = letter_byte.decode()
        websitename = websitename + letter_char



    myIp = ip_data(websitename)
    if (myIp.hex_IP == "0"):
        print("client ended session")
        quit()
    RDATA = IP.hex_IP
    if (myIp.hex_IP2 != "0"):
        RDATA2 = NAME + TYPE + CLASS + IP.TTL + IP.RDLENGTH + IP.hex_IP2
    else:
        RDATA2 = ""    

    # formulate response
    response = HEADER + NAME + TYPE + CLASS + IP.TTL + IP.RDLENGTH + RDATA + RDATA2  

    # ------------- send response ---------
    print("output:")
    print(response)

    msg_s2c = str.encode(response)
    ServerSocket.sendto(msg_s2c,client_IP)
