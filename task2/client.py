import socket
import random

def return_type (type):
    if(type=="00 01"):
        return " type A,"
    else :
        return " undefined type, "    

def return_class (cl):
    if(cl=="00 01"):
        return " class IN,"
    else: 
        return "undefined class" 

def return_ttl (cl):
    TTL = str(cl[:2])
    TTL = TTL + str(cl[3:5])
    return str(int(TTL, 16))   

def return_IP (hex,length):
    i=0
    IP = ""
    while (i<length):
        IP = IP + str(int(hex[i*3:2+i*3],16)) + "."
        i += 1
    return IP

#-----------open UDP socket (as client)----------------
server_add_port = ("127.0.0.1", 12000)
buffer_size = 2048
ClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

#----------generate message to send-----------

# generate header
ID = str(hex(random.randint(0,65535))) 
ID = ID[2:]
ID = ID.zfill(4)
IDn = ID[:2] + " " + ID[2:] + " "
FLAGS = "04 00 " #QR = 0,OPCODE = 0,AA = 1,TC=0,RD=0,RA=0, Z=000, RCODE=0 == ob0000010000000000 = ox 0400
QDCOUNT = "00 01 "
ANCOUNT = "00 00 " 
NSCOUNT = "00 00 "
ARCOUNT = "00 00 "

# get input (which website IP will be requested?)
print("input from the user:")
requested_website = input("> Enter Domain Name: ") 

#generate QNAME 
first_label_length = requested_website.find('.')
if (first_label_length == -1):
    first_label_length = len(requested_website)
second_label_length = len(requested_website) - first_label_length - 1
QNAME = "0" + str(first_label_length) +" "
for char in requested_website:
    if (char == '.'):
        cur_char = "0" + str(second_label_length) + " "
    else:
        cur_char = str(hex(ord(char))) + " "
        cur_char = cur_char[2:]
    QNAME = QNAME + cur_char
QNAME = QNAME + "00 "    

QTYPE ="00 01 "
QCLASS = "00 01 "

#formulate request 
request = IDn + FLAGS + QDCOUNT + ANCOUNT + NSCOUNT + ARCOUNT + QNAME + QTYPE + QCLASS
msg_c2s = str.encode(request)

#send generated message
ClientSocket.sendto(msg_c2s, server_add_port)

#-------------end session-----------------
if (requested_website == "end"):
    print("Session ended.")
    quit() 

#-------------get response----------------
msg_s2c = ClientSocket.recvfrom(buffer_size)

#-------------decode message -------------
response = "{}".format(msg_s2c[0])
response = response[2:]
TYPE = response[42:42+5]
CLASS = response[48:48+5]
TTL = response[54:54+5]
RDLENGTH = response[60:60+2]
RDATA = response[63:63+3*int(RDLENGTH, 16)]

#----------formulate print message -------
printmessage = requested_website + ":" + return_type(TYPE) + return_class(CLASS) + " TTL " + return_ttl(TTL) + ", addr (" + str(int(RDLENGTH, 16)) + ") " + return_IP(RDATA,int(RDLENGTH,16))


#-------------print message---------------
print("Output:")
print(printmessage)
if (len(response)>28*3+3*int(RDLENGTH, 16)):
    offset = 27+3*int(RDLENGTH, 16) #13*3 9*3+4*3
    TYPE = response[offset + 42:offset + 42+5]
    CLASS = response[offset + 48:offset + 48+5]
    TTL = response[offset + 54:offset + 54+5]
    RDLENGTH = response[offset + 60:offset + 60+2]
    RDATA = response[offset + 63:offset + 63+3*int(RDLENGTH, 16)]
    print(requested_website + ":" + return_type(TYPE) + return_class(CLASS) + " TTL " + return_ttl(TTL) + ", addr (" + str(int(RDLENGTH, 16)) + ") " + return_IP(RDATA,int(RDLENGTH,16)))


# git status to see what files you have modified
# git add filename to add file or git add . to add all files
# git commit -m "message" to make commit
# git push to push to repo
