from socket import *
import datetime
import os
import os.path

server_ip = "127.0.0.1"
server_port = 6789
server_socket = socket(AF_INET, SOCK_STREAM) # create TCP socket
server_socket.bind((server_ip, server_port)) # bind server to ip and port
server_socket.listen(1) # listen for incoming TCP request
print("The server is ready to receive requests")

def build_headers(response_code, path=''):
    header = []
    if response_code == 200:
        header.append('HTTP/1.1 200 OK\n')
    elif response_code == 404:
        header.append('HTTP/1.1 404 Not Found\r\n')
    current_time = datetime.datetime.now(datetime.timezone.utc).strftime('%a, %d %b %Y %H:%M:%S GMT')
    server_name = gethostname()
    if path == '':
        modified_time = current_time
        file_size = 0
    else:
        modified_time = datetime.datetime.utcfromtimestamp((os.path.getmtime(path))).strftime('%a, %d %b %Y %H:%M:%S GMT')
        file_size = os.path.getsize(path)
    header.append('Date: {time}\n'.format(time=current_time))
    header.append('Server: {name}\n'.format(name=server_name))
    header.append('Last-Modified: {last_time}\n'.format(last_time=modified_time))
    header.append('Content-Length: {length}\n'.format(length=file_size))
    header.append('Content-Type: text/html\n')
    header.append('Connection: keep-alive\n\n')

    return ''.join(header)

def build_response(header, r_data):
    response = header.encode()
    if request_type == "GET":
            response += r_data.encode()

    return response

# search through current directory and sub directories to see if file exists
def search_directories(file_name):
    file_path = ''
    for (dir,subdirs,files) in os.walk(os.getcwd()):
        if file_name in files:
            file_path = os.path.join(dir, file_name)
    return file_path

while True:
    client_socket, client_address = server_socket.accept()
    print('New connection from', client_address)
    data = client_socket.recv(2048).decode()

    if not data:
        break
    
    request_type = data.split(' ')[0]

    try:
        # check whether file exists in current or sub directories
        filename = data.split(' ')[1]
        
        file_path = search_directories(filename[1:])

        # open file and create file object
        file_obj = open(file_path)

        # read file
        r_data = file_obj.read()
        
        # close file object
        file_obj.close()

        r_header = build_headers(200, file_path)

        response = build_response(r_header, r_data)

        # send response
        client_socket.send(response)
        client_socket.close()
    except IOError:
        # ending up here means we couldn't find the requested file and so we want to return back a HTTP 404 Not Found Response message
        r_header = build_headers(404).encode()
        client_socket.send(r_header)
        client_socket.close()
server_socket.close()