# ECE 358 LAB 2
## Task 1: Web server that handles HTTP request.
In order to use the programms as expected in the lab manual run the server program using
```python web_server.py```
or
```python3 web_server.py```

Then on a client browser or HTTP client make a GET or HEAD request for a file.
eg.
```http://127.0.0.1:6789/HelloWorld.html```
